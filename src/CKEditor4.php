<?php

namespace Encore\CKEditor4;

use Encore\Admin\Extension;

class CKEditor4 extends Extension
{
    public $name = 'CKEditor4';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';

    public $menu = [
        'title' => 'Ckeditor4',
        'path'  => 'CKEditor4',
        'icon'  => 'fa-gears',
    ];
}