<?php

namespace Encore\CKEditor4;

use Encore\Admin\Form;
use Illuminate\Support\ServiceProvider;
use  Encore\Admin\Admin;

class CKEditor4ServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(CKEditor4 $extension)
    {
        if (! CKEditor4::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'CKEditor4');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/laravel-admin-ext/')],
                'CKEditor4'
            );
        }

//        $this->app->booted(function () {
//            CKEditor4::routes(__DIR__.'/../routes/web.php');
//        });

        Admin::booting(function () {
            Form::extend('editor', FormText::class);
            Admin::js('vendor/laravel-admin-ext/CKEditor4/ckeditor.js');
            Admin::js('vendor/laravel-admin-ext/CKEditor4/adapters/jquery.js');
        });
    }
}
