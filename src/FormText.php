<?php

namespace Encore\CKEditor4;

use Encore\Admin\Extension;
use Encore\Admin\Form\Field;

class FormText extends Field
{
    
    protected $view = 'CKEditor4::index';

    public function render()
    {
        $this->script = "$('textarea.{$this->getElementClassString()}').ckeditor();";

        return parent::render();
    }
}
