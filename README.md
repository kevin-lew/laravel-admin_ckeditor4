# laravel-admin CKEdit 文本域扩展
======
## 版本需求
- laravel 6.20
- laravel-admin 1.8.10
- CKEdit Full 4.19.0 

## 开始使用

### 安装
```bash
composer require kevinlew/laravel-admin-ckeditor4
```
### 发布资源
```bash
php artisan vendor:publish --provider=Encore\\CKEditor4\\CKEditor4ServiceProvider
```
### 注册扩展
> app/Admin/bootstrap.php 添加如下
```php
//如已注册editor则需要移除，否则会出现兼容问题
Form::forget(['editor']);
Form::extend('ckeditor4', \Encore\CKEditor4\FormText::class);
```


### 使用
```php
$form->ckeditor4('content','内容')
```

#### 配置文件上传
> 具体的配置直接在原生的config.js文件中配置，具体可以参考官方的文档:public/vendor/laravel-admin-ext/CKEditor4/config.js
```js
//配置文件上传的路径
config.filebrowserImageUploadUrl='';

```

#### 编写上传代码
```php
    /**
     * ——————————————————————————————————
     * #上传图片接口
     * ——————————————————————————————————
     * @API /sysadmin/api/upload
     * @method POST
     * @param Request $request
     * @return JsonResponse
     * @author kevinlew
     * @date 20/3/10 4:16 下午
     */

    public function upload(Request $request)
    {
        $file = $request->file('upload'); // get file
        $url=Storage::url($file->store('images'));
        // response
        $param = [
            'uploaded' => 1,
            'fileName' => 'fileName',
            'url' => $url
        ];
        return response()->json($param, 200);
    }
```

#### 展示效果

![img.png](/kevin-lew/laravel-admin_ckeditor4/raw/master/demo.png)